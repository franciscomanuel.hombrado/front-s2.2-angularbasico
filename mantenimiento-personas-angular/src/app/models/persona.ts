import { PersonaInputDto } from ".";

export class Persona{

    static personaOutputDto(personaInputDto: PersonaInputDto){
        return new Persona(
            personaInputDto['usuario'],
            personaInputDto['password'],
            personaInputDto['name'],
            personaInputDto['surname'],
            personaInputDto['company_email'],
            personaInputDto['personal_email'],
            personaInputDto['city'],
            personaInputDto['active'],
            personaInputDto['created_date'],
            personaInputDto['imagen_url'],
            personaInputDto['termination_date'],
        );
    }
    
    constructor(
        public usuario:          string,
        public password:         string,
        public name:             string,
        public surname:          string,
        public company_email:    string,
        public personal_email:   string,
        public city:             string,
        public active:           boolean,
        public created_date:     Date,
        public imagen_url:       string,
        public termination_date: Date
    ){}

}