import { Component, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import {MatTableDataSource} from '@angular/material/table';

import {  Persona, PersonaInputDto } from '../models';
import { PersonasService } from '../services/personas.service';
import { VentanaModalService } from '../services/ventana-modal.service';

@Component({
  selector: 'app-tabla-personas',
  templateUrl: './tabla-personas.component.html',
  styleUrls: ['./tabla-personas.component.css']
})
export class TablaPersonasComponent {

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns: string[] = ['name', 
                                'surname',
                                'botones'];

  personasTabla = new MatTableDataSource<PersonaInputDto>();

  constructor(private personaService: PersonasService,
              private ventanaModalService: VentanaModalService,
              private dialog: MatDialog
              ) {  }

  ngOnInit() : void{
    this.personaService.cargarPersonas().subscribe( (data) =>
      this.personasTabla = new MatTableDataSource<PersonaInputDto>(data)
    );
  }

  baja(id: string){
    this.personaService.deletePersona(id).subscribe( () => 
      this.ngOnInit()
    )
  }
  
  getDetalle(persona: Persona){
    this.ventanaModalService.cargarPersonaDetalle(persona, this.dialog)
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.personasTabla.filter = filterValue.trim().toLowerCase();
  }
}