import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { PersonasService } from '../services/personas.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Persona } from '../models';

@Component({
  selector: 'app-update',
  templateUrl: '../formulario.html',
  styleUrls: ['../formulario.css']
})
export class UpdateComponent implements OnInit {
  id: string = ''

  constructor(private route: ActivatedRoute,
              private personaService: PersonasService,
              private navigateRouter: Router) { }

  registerForm = new FormGroup({
    usuario: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
    name: new FormControl('', Validators.required),
    surname: new FormControl(''),
    company_email: new FormControl('', Validators.email),
    personal_email: new FormControl('', Validators.email),
    city: new FormControl('', Validators.required),
    active: new FormControl(''),
    created_date: new FormControl('', Validators.required),
    imagen_url: new FormControl(''),
    termination_date: new FormControl('')
  });

  ngOnInit(): void {    
    this.route.params.subscribe(params => {
      this.id = params['id_persona']
      this.personaService.cargarPersona(this.id).subscribe( (data) => {
        
        let persona = Persona.personaOutputDto(data)

        this.registerForm = new FormGroup({
          usuario: new FormControl(persona.usuario),
          password: new FormControl(persona.password),
          name: new FormControl(persona.name),
          surname: new FormControl(persona.surname),
          company_email: new FormControl(persona.company_email),
          personal_email: new FormControl(persona.personal_email),
          city: new FormControl(persona.city),
          active: new FormControl(persona.active),
          created_date: new FormControl(new Date(persona.created_date)),
          imagen_url: new FormControl(persona.imagen_url),
          termination_date: new FormControl(new Date(persona.termination_date))
        });
      })
    })        
  }

  onSubmit(){
    this.personaService.putPersona(this.id, Persona.personaOutputDto(this.registerForm.value)).subscribe( () =>{
      this.personaService.cargarPersonas();
      this.navigateRouter.navigateByUrl('init');
    });
  }
  
}
