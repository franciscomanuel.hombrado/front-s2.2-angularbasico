import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Persona } from '../models';
import { PersonasService } from '../services/personas.service';

@Component({
  selector: 'app-add',
  templateUrl: '../formulario.html',
  styleUrls: ['../formulario.css']
})
export class AddComponent implements OnInit {

  registerForm = new FormGroup({
    usuario: new FormControl(''),
    password: new FormControl(''),
    name: new FormControl(''),
    surname: new FormControl(''),
    company_email: new FormControl(''),
    personal_email: new FormControl(''),
    city: new FormControl(''),
    active: new FormControl(''),
    created_date: new FormControl(''),
    imagen_url: new FormControl(''),
    termination_date: new FormControl('')
  });

  constructor(private personaService: PersonasService,
              private navigateRouter: Router) { }

  ngOnInit(): void {
  }

  onSubmit(){
    this.personaService.postPersona(Persona.personaOutputDto(this.registerForm.value)).subscribe( () =>{ 
      this.personaService.cargarPersonas()
      this.navigateRouter.navigateByUrl('init')
    })
    
  }
}
