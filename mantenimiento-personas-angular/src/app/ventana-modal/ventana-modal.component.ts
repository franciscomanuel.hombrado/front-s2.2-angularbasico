import { Component, OnInit } from '@angular/core';

import {MatTableDataSource} from '@angular/material/table';

import {  PersonaInputDto } from '../models';
import { VentanaModalService } from '../services/ventana-modal.service';

@Component({
  selector: 'app-ventana-modal',
  templateUrl: './ventana-modal.component.html',
  styleUrls: ['./ventana-modal.component.css']
})
export class VentanaModalComponent implements OnInit {

  displayedColumns: string[] = ['usuario',
                                'password',
                                'name',
                                'surname',
                                'company_email',
                                'personal_email',
                                'city',
                                'active',
                                'created_date', 
                                'imagen_url',
                                'termination_date'];

  personaTablaDetalle = new MatTableDataSource<PersonaInputDto>();

  constructor(private ventanaModalService: VentanaModalService
              ) {  }

  ngOnInit() : void{
        this.personaTablaDetalle = new MatTableDataSource(this.ventanaModalService.getPersonaTablaDetalle())
  }

}
