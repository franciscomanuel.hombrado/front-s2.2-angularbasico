import { HttpClient} from '@angular/common/http';
import { Injectable} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Persona} from '../models';
import { VentanaModalComponent } from '../ventana-modal/ventana-modal.component';

@Injectable({  providedIn: 'root'})
export class VentanaModalService {
    personaDetalle: Persona[]

    constructor(private http: HttpClient) {
    this.personaDetalle = []
  }

  cargarPersonaDetalle(persona: Persona, dialog: MatDialog){
    this.personaDetalle[0] = persona
    dialog.open(VentanaModalComponent)
  }

  getPersonaTablaDetalle(){
    return this.personaDetalle;
  }

}