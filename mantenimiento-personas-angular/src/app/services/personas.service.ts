import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Injectable} from '@angular/core';
import { PersonaInputDto,  Persona} from '../models';

@Injectable({  providedIn: 'root'})
export class PersonasService {

  constructor(private http: HttpClient) { }

  cargarPersonas() {
    const URL = 'http://localhost:8080/persona';

    return this.http.get < PersonaInputDto[] > (URL)
      
  }

  cargarPersona(id_persona: string) {
    const URL = `http://localhost:8080/persona/${id_persona}`;

    return this.http.get < PersonaInputDto > (URL)
  }

  deletePersona(id: string){
    const URL = `http://localhost:8080/persona/${id}`;

    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    };

    return this.http.delete< PersonaInputDto[] >(URL, options)
  }

  putPersona(id: string, persona: Persona){
    const URL = `http://localhost:8080/persona/${id}`
    const body = JSON.stringify(persona)

    return this.http.put<Persona>(URL, body, { headers: { 'Content-Type': 'application/json' } })
  }

  postPersona(persona: Persona){
    const URL = "http://localhost:8080/persona"
    const body = JSON.stringify(persona)

    return this.http.post<Persona>(URL, body, { headers: { 'Content-Type': 'application/json' } })
  }
}
