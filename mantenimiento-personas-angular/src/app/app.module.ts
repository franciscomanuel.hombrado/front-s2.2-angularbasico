import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';

import { HttpClientModule } from '@angular/common/http';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';

import { MatDialogModule } from '@angular/material/dialog';
import { PersonasService } from './services/personas.service';
import { VentanaModalService } from './services/ventana-modal.service';
import { TablaPersonasComponent } from './tabla-personas/tabla-personas.component';
import { VentanaModalComponent } from './ventana-modal/ventana-modal.component';
import { UpdateComponent } from './update/update.component';
import { AddComponent } from './add/add.component';

import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  declarations: [
    AppComponent,
    TablaPersonasComponent,
    VentanaModalComponent,
    UpdateComponent,
    AddComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    HttpClientModule,
    MatInputModule,
    MatButtonModule,
    FormsModule,
    MatDialogModule,
    ReactiveFormsModule,
    FlexLayoutModule
  ],
  providers: [PersonasService, VentanaModalService],
  bootstrap: [AppComponent],
  entryComponents:[]
})
export class AppModule { }
