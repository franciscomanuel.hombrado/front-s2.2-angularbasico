import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddComponent } from './add/add.component';
import { TablaPersonasComponent } from './tabla-personas/tabla-personas.component';
import { UpdateComponent } from './update/update.component';

const routes: Routes = [
  
  {
    path: 'init', component: TablaPersonasComponent
  },

  {
    path: 'update/:id_persona', component: UpdateComponent
  },

  {
    path: 'add', component: AddComponent
  },

  {
    path: '', pathMatch: 'full', redirectTo: 'init'
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
